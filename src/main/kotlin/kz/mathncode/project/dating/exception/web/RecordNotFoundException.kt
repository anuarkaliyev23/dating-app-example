package kz.mathncode.project.dating.exception.web

import kz.mathncode.project.dating.model.persistent.Model
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

/**
 * Should be thrown if some entity was not found in the database
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
class RecordNotFoundException(val modelClass: KClass<out Model>, property: KProperty1<out Model, Any>, propertyValue: Any) : WebDatingAppException(
    message = "Record {${modelClass.simpleName}} was not found by property {$property} with value {$propertyValue}",
    cause = null,
    statusCode = HttpStatusCode.STATUS_CODE_404_NOT_FOUND
)