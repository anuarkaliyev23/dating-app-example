package kz.mathncode.project.dating.exception

/**
 * Base exception for Exception at the application level
 * of this application
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
open class DatingAppException(
    override val message: String?,
    override val cause: Throwable? = null,
) : Exception(message, cause)