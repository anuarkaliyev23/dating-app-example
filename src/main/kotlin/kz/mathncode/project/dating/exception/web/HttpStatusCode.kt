package kz.mathncode.project.dating.exception.web

/**
 * Simple Utility enum for HTTP Response status codes
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
enum class HttpStatusCode(val code: Int) {
    STATUS_CODE_200_OK(200),
    STATUS_CODE_201_CREATED(201),
    STATUS_CODE_204_NO_CONTENT(204),

    STATUS_CODE_400_BAD_REQUEST(400),
    STATUS_CODE_401_UNAUTHORIZED(401),
    STATUS_CODE_403_FORBIDDEN(403),
    STATUS_CODE_404_NOT_FOUND(404),

    STATUS_CODE_500_INTERNAL_SERVER_ERROR(500),
}