package kz.mathncode.project.dating.exception.web

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

/**
 * [WebDatingAppException] for cases when
 * there is an error with [providedValue] for
 * [modelProperty]
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
class JsonInvalidFieldException(
    val modelClass: KClass<*>,
    val modelProperty: KProperty1<*, Any?>,
    val providedValue: Any?,
    val mustBe: String,
    override val cause: Throwable? = null
) : WebDatingAppException(
    message = "Invalid JSON field for class {$modelClass} property {$modelProperty}. Value provided {$providedValue}. Must be {$mustBe}",
    cause = cause,
    statusCode = HttpStatusCode.STATUS_CODE_400_BAD_REQUEST
)