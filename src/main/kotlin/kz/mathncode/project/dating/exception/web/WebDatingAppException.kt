package kz.mathncode.project.dating.exception.web

import kz.mathncode.project.dating.exception.DatingAppException

/**
 * Any web-based [DatingAppException]
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
open class WebDatingAppException(
    override val message: String,
    override val cause: Throwable?,
    val statusCode: HttpStatusCode,
) : DatingAppException(message, cause)