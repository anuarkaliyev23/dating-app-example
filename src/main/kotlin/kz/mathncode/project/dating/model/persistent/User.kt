package kz.mathncode.project.dating.model.persistent

import com.fasterxml.jackson.annotation.JsonView
import kz.mathncode.project.dating.json.serialization.Views
import java.time.Instant
import java.time.LocalDate
import java.util.*
import javax.persistence.*

/**
 * Entity of the user class
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
@Entity
class User : Model {
    /**
     * Primary Key
     *
     * @since 0.1.0
     * */
    @field:Id
    @field:Column
    @field:JsonView(Views.Companion.UserView::class)
    override lateinit var id: UUID

    /**
     * User's first name
     *
     * @since 0.1.0
     * */
    @field:Column(name = COLUMN_FIRST_NAME)
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var firstName: String

    /**
     * User's last name
     *
     * @since 0.1.0
     * */
    @field:Column(name = COLUMN_LAST_NAME)
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var lastName: String

    /**
     * True if user is male
     * False if not
     *
     * @since 0.1.0
     * */
    @field:Column(name = COLUMN_IS_MALE)
    @field:JsonView(Views.Companion.UserView::class)
    var isMale: Boolean = false

    /**
     * User's birthday
     *
     * @since 0.1.0
     * */
    @field:Column
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var birthday: LocalDate

    /**
     * User's phone number
     *
     * @since 0.1.0
     * */
    @field:Column
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var phone: String

    /**
     * Bidirectional mapping for [Password] entity
     *
     * @since 0.1.0
     * */
    @OneToOne(mappedBy = Password.PROPERTY_USER, cascade = [CascadeType.ALL])
    @field:JsonView(Views.Companion.SystemView::class)
    lateinit var password: Password

    /**
     * User's address
     *
     * @since 0.1.0
     * */
    @field:Embedded
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var address: Address

    /**
     * Date of creation of a record
     *
     * @since 0.1.0
     * */
    @field:Column(name = Model.COLUMN_CREATED_AT)
    @field:JsonView(Views.Companion.UserView::class)
    override lateinit var createdAt: Instant

    companion object {
        const val COLUMN_FIRST_NAME = "first_name"
        const val COLUMN_LAST_NAME = "last_name"
        const val COLUMN_IS_MALE = "is_male"
    }
}