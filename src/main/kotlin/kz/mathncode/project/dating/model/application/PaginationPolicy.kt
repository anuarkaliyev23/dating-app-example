package kz.mathncode.project.dating.model.application

interface PaginationPolicy {
    val pageNumber: Int
    val pageSize: Int

    fun limit(): Int
    fun offset(): Int
}