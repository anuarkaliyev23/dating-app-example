package kz.mathncode.project.dating.model.application

/**
 * Simple implementation of [PaginationPolicy]
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
class BasicPaginationPolicy(
    /**
     * Number of the page
     *
     * @since 0.1.0
     * */
    override val pageNumber: Int = DEFAULT_PAGE_NUMBER,

    /**
     * Quantity of the records per page
     *
     * @since 0.1.0
     * */
    override val pageSize: Int = DEFAULT_PAGE_SIZE,
) : PaginationPolicy {


    override fun limit(): Int = pageSize

    override fun offset(): Int {
        return (pageNumber - 1) * pageSize
    }

    companion object {
        const val DEFAULT_PAGE_NUMBER = 1
        const val DEFAULT_PAGE_SIZE = 100
    }
}