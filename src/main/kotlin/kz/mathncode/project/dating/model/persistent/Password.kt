package kz.mathncode.project.dating.model.persistent

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonView
import kz.mathncode.project.dating.json.serialization.Views
import java.time.Instant
import java.util.*
import javax.persistence.*

/**
 * User's memorized authenticator as defined in NIST 800-63B
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
@Entity
class Password : Model {
    /**
     * Primary Key
     *
     * @since 0.1.0
     * */
    @field:Id
    @field:JsonView(Views.Companion.SystemView::class)
    override lateinit var id: UUID

    /**
     * Secret e.g. PIN/Password in the case of memorized authenticator
     * ATTENTION there are no means at this class for ensuring cryptographical
     * safety of this field.
     * BE CAUTIOUS using it. PROVIDE ALL NECESSARY MEANS
     * TO ENSURE SAFETY OF THIS FIELD ELSEWHERE
     *
     * @since 0.1.0
     * */
    @field:Column
    @field:JsonView(Views.Companion.SystemView::class)
    lateinit var secret: String

    /**
     * Foreign key to [User] entity
     * */
    @field:OneToOne
    @field:JoinColumn(referencedColumnName = Model.COLUMN_ID, name = COLUMN_USER)
    @field:JsonIgnore
    lateinit var user: User

    /**
     * Date of creation
     *
     * @since 0.1.0
     * */
    @field:Column(name = Model.COLUMN_CREATED_AT)
    @field:JsonView(Views.Companion.SystemView::class)
    override lateinit var createdAt: Instant

    companion object {
        const val PROPERTY_USER = "user"
        const val COLUMN_USER = "user_id"
    }
}