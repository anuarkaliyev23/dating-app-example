package kz.mathncode.project.dating.model.persistent

import com.neovisionaries.i18n.CountryCode
import javax.persistence.Column
import javax.persistence.Embeddable

/**
 * Separate class for address.
 * Although it is a separate class
 * it will be stored in the same table as an Entity is using them
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
@Embeddable
class Address() {
    /**
     * ISO 3166 code of the country
     *
     * @since 0.1.0
     * */
    @Column
    lateinit var country: CountryCode
    @Column
    lateinit var city: String
}