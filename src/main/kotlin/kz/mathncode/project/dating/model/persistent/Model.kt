package kz.mathncode.project.dating.model.persistent

import java.time.Instant
import java.util.*

/**
 * Base interface for all persistent models
 * It is implemented in interface, not in a abstract class
 * because it will be inconvenient to use base class as Entity
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
interface Model {
    /**
     * Primary key of a model
     *
     * @see UUID
     * @since 0.1.0
     * */
    val id: UUID

    /**
     * Creation date of an entity.
     * Symbolizes UNIX timestamp - seconds from 01.01.1970(GMT + 0)
     *
     * @see Instant
     * @since 0.1.0
     * */
    val createdAt: Instant

    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_CREATED_AT = "created_at"
    }
}