package kz.mathncode.project.dating.model.persistent

import com.fasterxml.jackson.annotation.JsonView
import kz.mathncode.project.dating.json.serialization.Views
import java.time.Instant
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Entity class for a meme
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
@Entity
class Meme() : Model {
    @field:Id
    override lateinit var id: UUID
    /**
     * Link to internet-resource with a meme
     *
     * @since 0.1.0
     * */
    @field:Column
    @field:JsonView(Views.Companion.UserView::class)
    lateinit var link: String

    @field:Column(name = Model.COLUMN_CREATED_AT)
    @field:JsonView(Views.Companion.UserView::class)
    override lateinit var createdAt: Instant


}