package kz.mathncode.project.dating.dao

import kz.mathncode.project.dating.model.application.PaginationPolicy
import kz.mathncode.project.dating.model.persistent.Model
import javax.persistence.EntityManager
import javax.transaction.Transaction
import kotlin.reflect.KProperty1

/**
 * Base interface for Data Access Object pattern implementatino
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
interface DAO<T, ID> {
    /**
     * Method returns all records from database
     *
     * @param entityManager - holds current transaction
     * @param paginationPolicy - holds pagination info
     *
     * @return List of all records using [paginationPolicy] data
     * @since 0.1.0
     * */
    fun findAll(entityManager: EntityManager, paginationPolicy: PaginationPolicy): List<T>

    /**
     * Method returning record by id
     *
     * @param entityManager - holds current transaction
     * @param id - primary key
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun findById(entityManager: EntityManager, id: ID): T?


    /**
     * Method for persisting an entity
     *
     * @param entityManager - holds current transaction
     * @param model - model need to be persisted
     *
     * @return the same entity with updated fields by JPA provider
     * @since 0.1.0
     * */
    fun persist(entityManager: EntityManager, model: T): T

    /**
     * Method returning record by field
     *
     * @param entityManager - holds current transaction
     * @param property - property of given class [T]
     * @param propertyValue - value of the given [property]
     * @param paginationPolicy - data for implementing pagination mechanism
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun<U> findBy(entityManager: EntityManager, property: KProperty1<out T, U>, propertyValue: U, paginationPolicy: PaginationPolicy): List<T>


    /**
     * Method returning record by field with unique value
     *
     * @param entityManager - holds current transaction
     * @param property - property of given class [T]
     * @param propertyValue - value of the given [property]
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun<U> findByUnique(entityManager: EntityManager, property: KProperty1<out T, U>, propertyValue: U): T?

    /**
     * Method for updating record [newModel] in database
     *
     * @param entityManager - holds current transaction
     * @param newModel - model with updated fields
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun update(entityManager: EntityManager, model: T) : T

    /**
     * Method for deleting record [model] in database
     *
     * @param entityManager - holds current transaction
     * @param model - model with updated fields
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun delete(entityManager: EntityManager, model: T)

    /**
     * Method for deleting record [model] in database by primary key
     *
     * @param entityManager - holds current transaction
     * @param id - record's primary key
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun deleteById(entityManager: EntityManager, id: ID)
}