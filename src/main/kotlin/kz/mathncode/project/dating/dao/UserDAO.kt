package kz.mathncode.project.dating.dao

import kz.mathncode.project.dating.model.persistent.User
import org.springframework.stereotype.Repository

@Repository
class UserDAO : ModelDAO<User>(User::class)