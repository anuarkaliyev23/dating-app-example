package kz.mathncode.project.dating.dao

import kz.mathncode.project.dating.model.persistent.Password
import org.springframework.stereotype.Repository

@Repository
class PasswordDAO : ModelDAO<Password>(Password::class)