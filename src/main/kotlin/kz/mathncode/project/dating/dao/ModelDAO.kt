package kz.mathncode.project.dating.dao

import kz.mathncode.project.dating.model.application.PaginationPolicy
import kz.mathncode.project.dating.model.persistent.Model
import org.springframework.stereotype.Repository
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.criteria.Selection
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

/**
 * Basic implementation of [DAO] for [Model] class
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
abstract class ModelDAO<T: Model>(
    val modelClass: KClass<out T>
) : DAO<T, UUID> {
    override fun findAll(entityManager: EntityManager, paginationPolicy: PaginationPolicy): List<T> {
        val builder = entityManager.criteriaBuilder
        val query = builder.createQuery(modelClass.java)
        val root = query.from(modelClass.java)
        query.select(root as Selection<out Nothing>)

        return entityManager.createQuery(query).setFirstResult(paginationPolicy.offset()).setMaxResults(paginationPolicy.limit()).resultList
    }

    override fun findById(entityManager: EntityManager, id: UUID): T? {
        return entityManager.find(modelClass.java, id)
    }

    override fun <U> findBy(
        entityManager: EntityManager,
        property: KProperty1<out T, U>,
        propertyValue: U,
        paginationPolicy: PaginationPolicy
    ): List<T> {
        val offset = paginationPolicy.offset()
        val limit = paginationPolicy.limit()
        val table = modelClass.simpleName

        val query = entityManager.createQuery("""
            SELECT *
            FROM :$SQL_TABLE
            WHERE :$SQL_PARAMETER = :$SQL_VALUE
        """.trimIndent())
            .setParameter(SQL_TABLE, table)
            .setParameter(SQL_PARAMETER, property)
            .setParameter(SQL_VALUE,propertyValue)
            .setParameter(SQL_OFFSET, offset)
            .setParameter(SQL_LIMIT, limit)

        return query.setFirstResult(paginationPolicy.offset()).setMaxResults(paginationPolicy.limit()).resultList as List<T>

    }

    override fun <U> findByUnique(entityManager: EntityManager, property: KProperty1<out T, U>, propertyValue: U): T? {
        val table = modelClass.simpleName

        val query = entityManager.createQuery("""
            SELECT *
            FROM :$SQL_TABLE
            WHERE :$SQL_PARAMETER = :$SQL_VALUE
        """.trimIndent())
            .setParameter(SQL_TABLE, table)
            .setParameter(SQL_PARAMETER, property)
            .setParameter(SQL_VALUE,propertyValue)

        return query.singleResult as T?
    }

    override fun update(entityManager: EntityManager, newModel: T) : T {
        entityManager.merge(newModel)
        return newModel
    }

    override fun delete(entityManager: EntityManager, model: T) {
        entityManager.remove(model)
    }

    override fun deleteById(entityManager: EntityManager, id: UUID) {
        val model = this.findById(entityManager, id)
        entityManager.remove(model)
    }

    override fun persist(entityManager: EntityManager, model: T): T {
        entityManager.persist(model)
        return model
    }

    companion object {
        const val SQL_OFFSET = "offset"
        const val SQL_LIMIT = "limit"
        const val SQL_TABLE = "table"

        const val SQL_PARAMETER = "parameter"
        const val SQL_VALUE = "value"
    }

}