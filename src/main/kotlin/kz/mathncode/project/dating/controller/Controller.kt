package kz.mathncode.project.dating.controller

import kz.mathncode.project.dating.model.application.BasicPaginationPolicy
import kz.mathncode.project.dating.model.application.PaginationPolicy
import kz.mathncode.project.dating.model.persistent.Model
import kz.mathncode.project.dating.service.ModelService
import java.util.*


interface Controller<T: Model> {
    fun getAll(page: Int, pageSize: Int): List<T>
    fun get(id: UUID): T
    fun post(model: T): T
    fun patch(id: UUID, model: T): T
    fun delete(id: UUID)

    fun paginationPolicy(page: Int, pageSize: Int): PaginationPolicy {
        return BasicPaginationPolicy(page, pageSize)
    }
}