package kz.mathncode.project.dating.controller

import kz.mathncode.project.dating.controller.UserController.Companion.MAPPING_PATH
import kz.mathncode.project.dating.exception.web.RecordNotFoundException
import kz.mathncode.project.dating.model.persistent.Model
import kz.mathncode.project.dating.model.persistent.User
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping(MAPPING_PATH)
class UserController : ModelController<User>() {
    override fun getAll(@RequestParam page: Int,@RequestParam pageSize: Int): List<User> {
        val paginationPolicy = paginationPolicy(page, pageSize)
        return service.findAll(paginationPolicy)
    }

    override fun get(id: UUID): User {
        return service.findByIdOrNull(id) ?: throw RecordNotFoundException(User::class, User::id, id)
    }

    override fun post(model: User): User {
        return service.persist(model)
    }

    override fun patch(id: UUID, model: User): User {
        model.id = id
        service.update(model)
        return model
    }

    override fun delete(id: UUID) {
        service.deleteById(id)
    }

    companion object {
        const val MAPPING_PATH = "users"
    }
}