package kz.mathncode.project.dating.controller

import kz.mathncode.project.dating.model.persistent.Model
import kz.mathncode.project.dating.service.ModelService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController

@RestController
abstract class ModelController<T: Model> : Controller<T> {
    @field:Autowired
    lateinit var service: ModelService<T>
}