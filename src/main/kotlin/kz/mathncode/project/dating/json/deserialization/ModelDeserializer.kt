package kz.mathncode.project.dating.json.deserialization

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.NullNode
import kz.mathncode.project.dating.exception.web.JsonInvalidFieldException
import kz.mathncode.project.dating.model.persistent.Model
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1

/**
 * Parent class for any [StdDeserializer] for deserializing any [Model] class
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
abstract class ModelDeserializer<T: Model>(val modelClass: KClass<T>) : StdDeserializer<T>(modelClass.java) {
    /**
     * Simple utility function to extract root of [parser] object
     *
     * @since 0.1.0
     * */
    fun root(parser: JsonParser) {
        parser.codec.readTree<JsonNode>(parser)
    }

    /**
     * Utility function to extract value of class [U] from [node] using [extractionFunction].
     *
     * @param node - [JsonNode] from which value is need to be extracted
     * @param property - [KProperty1] of the [modelClass]
     * @param extractionFunction - function need that is responsible for extracting property from [node]
     *
     * @throws JsonInvalidFieldException if value is null or [NullNode]
     *
     * @since 0.1.0
     * */
    fun<U> requiredProperty(node: JsonNode, property: KProperty1<T, Any>, extractionFunction: (JsonNode) -> U): U {
        val value = extractionFunction(node)
        if (value is NullNode || value == null) {
            throw JsonInvalidFieldException(modelClass, property, value, "Not null")
        }
        return value
    }

    /**
     * Utility function to extract value of class [U] from [node] using [extractionFunction].
     * If such value is null [defaultValue] will be returned
     *
     * @param node - [JsonNode] from which value is need to be extracted
     * @param property - [KProperty1] of the [modelClass]
     * @param extractionFunction - function need that is responsible for extracting property from [node]
     * @param defaultValue - value that will be returned in case of omitting this value in provided [node]
     *
     * @since 0.1.0
     * */
    fun<U> optionalProperty(node: JsonNode, property: KProperty1<T, Any>, extractionFunction: (JsonNode) -> U?, defaultValue: U?): U? {
        val value = extractionFunction(node) ?: defaultValue
        return value
    }

    /**
     * Function responsible for validation some [value] using [validatorFunction]
     *
     * @param value - provided value
     * @param validatorFunction - function that must validate value against some condition
     * @param property - [KProperty1] of the [modelClass]
     * @param mustBeMessage - message for the [JsonInvalidFieldException] in case validation doesn't checks out
     *
     * @throws JsonInvalidFieldException in case of the failed validation.
     *
     * @since 0.1.0
     * */
    fun<U> validate(value: U, validatorFunction: (U) -> Boolean, property: KProperty1<T, Any>, mustBeMessage: String) {
        if (!validatorFunction(value)) {
            throw JsonInvalidFieldException(modelClass, property, value, mustBeMessage)
        }
    }
}