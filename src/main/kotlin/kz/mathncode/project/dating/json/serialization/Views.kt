package kz.mathncode.project.dating.json.serialization

/**
 * Simple Views for JsonView mechanism.
 * It will help serialization process.
 *
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 *
 * @see [com.fasterxml.jackson.annotation.JsonView]
 * */
class Views {
    companion object {
        /**
         * Class for annotating fields that will be accessible for user
         *
         * @since 0.1.0
         * @author Khambar Dussaliyev
         * */
        open class UserView

        /**
         * Class for annotating fields that will only be accessible for
         * system.
         * It extends [UserView] class for System to be able to see all [UserView]
         * annotated fields
         *
         * @since 0.1.0
         * @author Khambar Dussaliyev
         * */
        open class SystemView : UserView()
    }
}