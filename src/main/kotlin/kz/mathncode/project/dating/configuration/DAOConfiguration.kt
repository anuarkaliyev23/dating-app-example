package kz.mathncode.project.dating.configuration

import kz.mathncode.project.dating.dao.DAO
import kz.mathncode.project.dating.dao.PasswordDAO
import kz.mathncode.project.dating.dao.UserDAO
import kz.mathncode.project.dating.model.persistent.Password
import kz.mathncode.project.dating.model.persistent.User
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*

@Configuration
open class DAOConfiguration {
    @Bean
    open fun userDAO(): DAO<User, UUID> {
        return UserDAO()
    }

    @Bean
    open fun passwordDAO(): DAO<Password, UUID> {
        return PasswordDAO()
    }
}