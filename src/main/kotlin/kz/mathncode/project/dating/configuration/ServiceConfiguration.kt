package kz.mathncode.project.dating.configuration

import kz.mathncode.project.dating.service.PasswordService
import kz.mathncode.project.dating.service.UserService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ServiceConfiguration {
    @Bean
    open fun userService(): UserService {
        return UserService()
    }

    @Bean
    open fun passwordService() : PasswordService {
        return PasswordService()
    }
}