package kz.mathncode.project.dating.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence

@Configuration
@EnableWebMvc
open class ApplicationConfiguration {
    @Bean
    open fun entityManagerFactory(): EntityManagerFactory {
        return Persistence.createEntityManagerFactory(PERSISTENCE_UNIT)
    }

    companion object {
        const val PERSISTENCE_UNIT = "dating-development"
    }
}