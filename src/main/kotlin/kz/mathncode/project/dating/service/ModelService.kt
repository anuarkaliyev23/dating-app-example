package kz.mathncode.project.dating.service

import kz.mathncode.project.dating.dao.DAO
import kz.mathncode.project.dating.model.persistent.Model
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import javax.persistence.EntityManagerFactory

/**
 * Basic implementation of [Service] for [Model]s
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
@org.springframework.stereotype.Service
abstract class ModelService<T: Model> : Service<T, UUID> {
    override lateinit var dao: DAO<T, UUID>
    override lateinit var entityManagerFactory: EntityManagerFactory
}