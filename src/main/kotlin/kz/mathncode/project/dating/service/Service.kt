package kz.mathncode.project.dating.service

import kz.mathncode.project.dating.dao.DAO
import kz.mathncode.project.dating.exception.web.RecordNotFoundException
import kz.mathncode.project.dating.model.application.PaginationPolicy
import kz.mathncode.project.dating.model.persistent.Model
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import kotlin.reflect.KProperty1

/**
 * Service contains all domain-specific logic
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
@Service
interface Service<T: Model, ID> {
    val dao: DAO<T, ID>
    val entityManagerFactory: EntityManagerFactory

    /**
     * Method returns all records from database
     *
     * @param paginationPolicy - holds pagination info
     *
     * @return List of all records using [paginationPolicy] data
     * @since 0.1.0
     * */
    fun findAll(paginationPolicy: PaginationPolicy): List<T> {
        return transaction {
            dao.findAll(it, paginationPolicy)
        }
    }

    /**
     * Method returning record by id or null if such record not exists
     *
     * @param id - primary key
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun findByIdOrNull(id: ID): T? {
        return transaction {
            dao.findById(it, id)
        }
    }

    /**
     * Method returning record by id
     *
     * @param id - primary key
     *
     * @return model of class [T] if such record exists.
     * @throws RecordNotFoundException if model was not found
     * @since 0.1.0
     * */
    fun findById(id: ID): T

    /**
     * Method returning record by field with unique value
     *
     * @param property - property of given class [T]
     * @param propertyValue - value of the given [property]
     *
     * @return model of class [T] if such record exists
     * @throws RecordNotFoundException if model was not found
     * @since 0.1.0
     * */
    fun<U> findByUnique(property: KProperty1<out T, U>, propertyValue: U) : T

    /**
     * Method returning record by field
     *
     * @param property - property of given class [T]
     * @param propertyValue - value of the given [property]
     * @param paginationPolicy - data for implementing pagination mechanism
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun<U> findBy(property: KProperty1<out T, U>, propertyValue: U, paginationPolicy: PaginationPolicy): List<T> {
        return transaction {
            dao.findBy(it, property, propertyValue, paginationPolicy)
        }
    }


    /**
     * Method returning record by field with unique value
     *
     * @param property - property of given class [T]
     * @param propertyValue - value of the given [property]
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun<U> findByUniqueOrNull(property: KProperty1<out T, U>, propertyValue: U): T? {
        return transaction {
            dao.findByUnique(it, property, propertyValue)
        }
    }

    /**
     * Method persists record in the database
     *
     * @param model - model needed to be persisted
     *
     * @return the same model with updated fields by JPA provider
     * @since 0.1.0
     * */
    fun persist(model: T) : T {
        return transaction {
            dao.persist(it, model)
        }
    }

    /**
     * Method for updating record [newModel] in database
     *
     * @param model - model with updated fields
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun update(model: T) {
        return transaction {
            dao.update(it, model)
        }
    }

    /**
     * Method for deleting record [model] in database
     *
     * @param model - model with updated fields
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun delete(model: T) {
        return transaction {
            dao.delete(it, model)
        }
    }

    /**
     * Method for deleting record [model] in database by primary key
     *
     * @param id - record's primary key
     *
     * @return model of class [T] if such record exists. Null - otherwise
     * @since 0.1.0
     * */
    fun deleteById(id: ID) {
        return transaction {
            dao.deleteById(it, id)
        }
    }


    /**
     * Simple wrapper function to wrap [action] function into Transaction
     * */
    fun<U> transaction(action: (em: EntityManager) -> U): U {
        val entityManager = entityManagerFactory.createEntityManager()
        entityManager.transaction.begin()
        val result = action(entityManager)
        entityManager.transaction.commit()
        return result
    }
}