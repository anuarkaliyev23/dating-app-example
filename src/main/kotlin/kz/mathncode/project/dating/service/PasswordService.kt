package kz.mathncode.project.dating.service

import kz.mathncode.project.dating.exception.web.RecordNotFoundException
import kz.mathncode.project.dating.model.persistent.Model
import kz.mathncode.project.dating.model.persistent.Password
import java.util.*
import kotlin.reflect.KProperty1

class PasswordService : ModelService<Password>() {
    override fun findById(id: UUID): Password {
        return findByIdOrNull(id) ?: throw RecordNotFoundException(Password::class, Password::id, id)
    }

    override fun <U> findByUnique(property: KProperty1<out Password, U>, propertyValue: U): Password {
        return findByUniqueOrNull(property, propertyValue) ?: throw RecordNotFoundException(Password::class, property as KProperty1<out Model, Any>, propertyValue as Any)
    }
}