package kz.mathncode.project.dating.service

import kz.mathncode.project.dating.dao.DAO
import kz.mathncode.project.dating.dao.UserDAO
import kz.mathncode.project.dating.exception.web.RecordNotFoundException
import kz.mathncode.project.dating.model.persistent.Model
import kz.mathncode.project.dating.model.persistent.User
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
import javax.persistence.EntityManagerFactory
import kotlin.reflect.KProperty1

@org.springframework.stereotype.Service
class UserService : ModelService<User>() {
    @field:Autowired
    override lateinit var dao: DAO<User, UUID>
    @field:Autowired
    override lateinit var entityManagerFactory: EntityManagerFactory

    override fun findById(id: UUID): User {
        return findByIdOrNull(id) ?: throw RecordNotFoundException(User::class, User::id, id)
    }

    override fun <U> findByUnique(property: KProperty1<out User, U>, propertyValue: U): User {
        return findByUniqueOrNull(property, propertyValue) ?: throw RecordNotFoundException(User::class, property as KProperty1<out Model, Any>, propertyValue as Any)
    }
}